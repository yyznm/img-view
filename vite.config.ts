import { resolve } from 'path'
import { defineConfig } from 'vite'
import dts from 'vite-plugin-dts'

export default defineConfig({
    build: {
        lib: {
            // 入口
            entry: resolve(__dirname, "src/main.ts"),
            name: "YYZIMG",
            fileName: (format) => `yyz-img.${format}.js`
        },
    },
    plugins: [
      dts({
          //指定使用的tsconfig.json为
          tsConfigFilePath: './tsconfig.json'
      })
  ]
});
