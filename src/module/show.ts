import YYZIMG, { OpType } from "./index";

export default function show(url: string, op?: OpType) {
    return new YYZIMG(url, op).show();
}
