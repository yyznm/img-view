# 图片查看

使用原生js编写，原生项目、vue、react等都能使用

## 介绍

js 图片查看,缩放,移动,旋转;  
dome 地址：[https://dome.yyzhu.vip/img-view](https://dome.yyzhu.vip/img-view)

## 使用

### ESModules方式

先安装

> npm i @yyznm/img-view
```javaScript
// 引入
import "@yyznm/img-view/dist/style.css"; // 样式
import YYZIMG, { show } from "@yyznm/img-view";
// 第一种，show方法有返回this，所以可以链式调用
const img1 = new YYZIMG("url", {/* 配置项 */}).show();
// 第二种, 直接显示并返回YYZIMG对象，注意，这个show方法和YYZIMG上的show方法不是同一个
const img2 = show("url", {/* 配置项 */})
```
### 原生js
```html
<link type="text/css" rel="styleSheet" href="https://cdn.jsdelivr.net/npm/@yyznm/img-view/dist/style.css" />
<script src="https://cdn.jsdelivr.net/npm/@yyznm/img-view"></script>
<script>
// 这里需要调用default，其他和ESModules方式一致
const img1 = new YYZIMG.default("url", {/* 配置项 */}).show()
// 或
const img2 = YYZIMG.show("url", {/* 配置项 */})
</script>
```

## 配置说明

### 配置项

|参数名|类型|说明|默认值|
|--|--|--|--|
| body | dom或选择器 | 要插入的dom, 默认的为body，注意:传入undefined也会使用的默认值，但传入null时将报错 | document.body |
| zIndex | number | 层级 |  1211 |
| maskColor | string | 遮罩颜色 | rgba(0,0,0,0.2) |
| startZoom | number | 初始化的缩放级别 | 1 |
| minZoom | number | 最小的缩放级别 | 0.1 |
| maxZoom | number | 最大的缩放级别 | 20(2000%) |
| isImgClose | boolean | 点击图片是否关闭 | true |
| isMaskClose | boolean | 点击遮罩是否关闭，开启遮罩移动(isMaskMove)后会跟随isImgClose的设置 | false |
| isMaskWheel | boolean | 开启遮罩缩放 | true |
| isMaskMove | boolean | 开启遮罩移动 | true |
| isOperation | boolean | 是否显示操作按钮 | true |
| tipTime | number | 放大倍数的显示时间(为0则不显示) | null |
| backCall | function | 关闭的回调(动画完成后回调) | null |
| showCall | function | 显示的回调 | null |

### 方法

> YYZIMG类上的方法

|方法名|说明|
|--|--|
| show() | 显示 |
| close() | 隐藏 |
| rotateLeft() | 左旋转90度 |
| rotateRight() | 右旋转90度 |
| zoomAdd() | 放大 |
| zoomSub() | 缩小 |
| reset() | 重置 |

## 样式

提示、操作栏等样式都可以覆盖原来的样式来达到你想要的效果，相信这对于作为前端大佬的你不是问题